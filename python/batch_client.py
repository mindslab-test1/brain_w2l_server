import argparse
import os
import re
import time

from functools import partial
from glob import glob
from utils import parallel_run, ler, wer
from client import W2lClient, read_pcm


def test(wav, w2l, input_dir, lang, use_txt, encoding, output, streaming):
    start_time = time.perf_counter()
    w2l_client = W2lClient(remote=w2l)
    if streaming:
        prediction = ""
        pcm_binary = read_pcm(wav)
        for pcm_idx, segment in enumerate(w2l_client.stream_recognize(pcm_binary)):
            prediction += segment.txt
            prediction += " "
        prediction = prediction.strip()
    else:
        with open(wav, 'rb') as rf:
            wav_binary = rf.read()
        prediction = w2l_client.recognize(wav_binary)
        prediction = prediction.txt

    if output is not None:
        output_path = re.sub(r"^{}".format(input_dir), "", wav)
        output_path = re.sub(r"^/", "", output_path)
        output_path = os.path.join(output, output_path)
        output_path = '{}.result'.format(os.path.splitext(output_path)[0])

        with open(output_path, 'w', encoding=encoding) as wf:
            wf.write(prediction)

    if use_txt:
        txt_path = '{}.txt'.format(os.path.splitext(wav)[0])
        with open(txt_path, 'r', encoding=encoding) as rf:
            target = rf.read()

        letter_err_cnt, letter_target_cnt = ler(prediction, target, lang)
        word_err_cnt, word_target_cnt = wer(prediction, target, lang)
        if letter_target_cnt == 0:
            letter_target_cnt = 1
        if word_target_cnt == 0:
            word_target_cnt = 1

        print('ler: {:.2f}%, wer: {:.2f}%, file: {}'.format(
            letter_err_cnt / letter_target_cnt * 100,
            word_err_cnt / word_target_cnt * 100,
            wav)
        )
        proc_time = time.perf_counter() - start_time
        return letter_err_cnt, letter_target_cnt, word_err_cnt, word_target_cnt, proc_time
    else:
        print('{}: {}'.format(prediction, wav))


def test_batch(wav_list, n_cpu, **kargv):
    test_fn = partial(test, **kargv)

    results = parallel_run(test_fn, wav_list,
                           desc="test", n_cpu=n_cpu)
    if len(results) > 0:
        letter_err_tot_cnt = 0
        letter_target_tot_cnt = 0
        word_err_tot_cnt = 0
        word_target_tot_cnt = 0
        proc_time_tot = 0
        for letter_err_cnt, letter_target_cnt, word_err_cnt, word_target_cnt, proc_time in results:
            letter_err_tot_cnt += letter_err_cnt
            letter_target_tot_cnt += letter_target_cnt
            word_err_tot_cnt += word_err_cnt
            word_target_tot_cnt += word_target_cnt
            proc_time_tot += proc_time
        letter_err_rate = letter_err_tot_cnt / letter_target_tot_cnt
        word_err_rate = word_err_tot_cnt / word_target_tot_cnt
        proc_time = proc_time_tot / n_cpu
        print('letter error rate: {:.2f}%, word error rate: {:.2f}%, processing time: {}s'.format(
            letter_err_rate * 100,
            word_err_rate * 100,
            proc_time
        ))
        return letter_err_rate, word_err_rate, proc_time


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--w2l',
                        nargs='?',
                        help='w2l grpc server=ip:port',
                        type=str,
                        default='172.17.0.1:15002')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input dir',
                        type=str,
                        required=True)
    parser.add_argument('-l', '--lang',
                        type=str,
                        default='kor',
                        help='language(kor, eng)')
    parser.add_argument('-ut', '--use_txt',
                        action='store_true',
                        help='use target txt file')
    parser.add_argument('-e', '--encoding',
                        type=str,
                        default='utf-8',
                        help='txt file encoding')
    parser.add_argument('-o', '--output',
                        nargs='?',
                        help='output dir',
                        type=str,
                        default='output')
    parser.add_argument('-s', '--streaming',
                        action='store_true',
                        help='use streaming recognize')
    parser.add_argument('-n', '--n_cpu',
                        type=int,
                        default=1,
                        help='number of processes')

    args = parser.parse_args()

    input_path = os.path.join(args.input, '**/*.wav')
    wav_list = glob(input_path, recursive=True)

    if not os.path.exists(args.output):
        os.mkdir(args.output)

    test_batch(wav_list, args.n_cpu,
               w2l=args.w2l, input_dir=args.input, lang=args.lang, use_txt=args.use_txt,
               encoding=args.encoding, output=args.output, streaming=args.streaming)


if __name__ == '__main__':
    main()
