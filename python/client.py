import argparse
import grpc
import wave

from maum.brain.w2l.w2l_pb2_grpc import SpeechToTextStub
from maum.brain.w2l.w2l_pb2 import Speech


def read_pcm(wav_file):
    with wave.open(wav_file, 'rb') as rf:
        pcm_binary = bytearray()
        while True:
            data = rf.readframes(16 * 1024)
            if len(data) == 0:
                break
            pcm_binary.extend(data)
    pcm_binary = bytes(pcm_binary)
    return pcm_binary


class W2lClient(object):
    def __init__(self, remote='127.0.0.1:15001', chunk_size=64*1024):
        self.channel = grpc.insecure_channel(remote)
        self.stub = SpeechToTextStub(self.channel)
        self.chunk_size = chunk_size

    def __del__(self):
        self.channel.close()

    def recognize(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.Recognize(wav_binary)

    def stream_recognize(self, pcm_binary):
        pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
        return self.stub.StreamRecognize(pcm_binary)

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield Speech(bin=wav_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    def main():
        parser = argparse.ArgumentParser(
            description='w2l client')
        parser.add_argument('-r', '--remote',
                            nargs='?',
                            dest='remote',
                            help='grpc: ip:port',
                            type=str,
                            default='127.0.0.1:15001')
        parser.add_argument('-i', '--input',
                            nargs='?',
                            help='input wav file',
                            type=str,
                            required=True)
        parser.add_argument('-s', '--streaming',
                            action='store_true',
                            help='use streaming recognize')

        args = parser.parse_args()

        client = W2lClient(args.remote)

        if args.streaming:
            pcm_binary = read_pcm(args.input)
            for pcm_idx, segment in enumerate(client.stream_recognize(pcm_binary)):
                print(segment.start, segment.end, segment.txt)
        else:
            with open(args.input, 'rb') as rf:
                wav_binary = rf.read()
                text = client.recognize(wav_binary)
            print(text.txt)

    main()
