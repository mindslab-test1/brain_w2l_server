import os
import argparse
import subprocess
import random

from glob import glob

from batch_client import test_batch

SERVER_CMD = '/opt/flashlight/bin/fl_asr_server'

def run_server(server_cfg,
               lm_weight, word_score, sil_score, beam_size, beam_threshold):
    server_process = subprocess.Popen(
        [
            SERVER_CMD,
            '--flagsfile', server_cfg,
            '--port', '15001',
            '--logtostderr', '1',
            '--v', '0',
            '--lmweight', str(lm_weight),
            '--wordscore', str(word_score),
            '--silscore', str(sil_score),
            '--beamsize', str(beam_size),
            '--beamthreshold', str(beam_threshold),
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True
    )
    while server_process.poll() is None:
        server_log = server_process.stdout.readline()
        if server_log.endswith('Server listening on 0.0.0.0:15001\n'):
            break
    return server_process


def stop_server(server_process):
    server_process.kill()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--lmweight', type=float, default=[0.0, 5.0],
                        nargs=2, help='lmweight start, stop, step')
    parser.add_argument('--wordscore', type=float, default=[0.0, 5.0],
                        nargs=2, help='wordscore start, stop, step')
    parser.add_argument('--silscore', type=float, default=[-3.0, 0.0],
                        nargs=2, help='silscore start, stop, step')
    parser.add_argument('--beamsize', type=int, default=[100, 250],
                        nargs=2, help='beamsize start, stop, step')
    parser.add_argument('--beamthreshold', type=int, default=[5, 7],
                        nargs=2, help='beamthreshold start, stop, step')
    parser.add_argument('--sample', type=int, default=1000,
                        help='random sample count')
    parser.add_argument('--server_cfg', type=str, required=True,
                        help='server.cfg path')
    parser.add_argument('--input', type=str, required=True,
                        help='input dir')
    parser.add_argument('--output', type=str, default='output.txt',
                        help='output path')
    parser.add_argument('--lang', type=str, default='kor',
                        help='language(kor, eng)')
    parser.add_argument('--encoding', type=str, default='utf-8',
                        help='txt file encoding')
    parser.add_argument('--n_cpu', type=int, default=1,
                        help='number of processes')
    args = parser.parse_args()

    input_path = os.path.join(args.input, '**/*.wav')
    wav_list = glob(input_path, recursive=True)

    if not os.path.exists(args.output):
        with open(args.output, 'w', encoding='utf-8') as wf:
            wf.write('lm_weight\tword_score\tsil_score\tbeam_size\tbeam_threshold\t')
            wf.write('ler\twer\ttime\n')

    for _ in range(args.sample):
        lm_weight = random.uniform(*args.lmweight)
        word_score = random.uniform(*args.wordscore)
        sil_score = random.uniform(*args.silscore)
        beam_size = random.randint(*args.beamsize)
        beam_threshold = random.randint(*args.beamthreshold)
        server_process = run_server(
            args.server_cfg, lm_weight, word_score, sil_score, beam_size, beam_threshold)
        letter_err_rate, word_err_rate, proc_time = test_batch(
            wav_list, args.n_cpu,
            w2l='127.0.0.1:15001', input_dir=None, lang=args.lang, use_txt=True,
            encoding=args.encoding, output=None, streaming=True)
        with open(args.output, 'a', encoding='utf-8') as wf:
            wf.write(f'{lm_weight}\t{word_score}\t{sil_score}\t{beam_size}\t{beam_threshold}\t')
            wf.write(f'{letter_err_rate}\t{word_err_rate}\t{proc_time}\n')
        stop_server(server_process)


if __name__ == '__main__':
    main()
