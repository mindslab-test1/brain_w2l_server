#include <cstdlib>
#include <csignal>
#include <fstream>
#include <iomanip>
#include <mutex>
#include <string>
#include <vector>
#include <regex>

#include "flashlight/fl/flashlight.h"
#include <gflags/gflags.h>
#include <glog/logging.h>

#include "flashlight/app/asr/common/Defines.h"
#include "flashlight/app/asr/common/Flags.h"
#include "flashlight/app/asr/criterion/criterion.h"
#include "flashlight/app/asr/data/FeatureTransforms.h"
#include "flashlight/app/asr/data/Sound.h"
#include "flashlight/app/asr/data/Utils.h"
#include "flashlight/app/asr/decoder/ConvLmModule.h"
#include "flashlight/app/asr/decoder/DecodeUtils.h"
#include "flashlight/app/asr/decoder/Defines.h"
#include "flashlight/app/asr/decoder/TranscriptionUtils.h"
#include "flashlight/app/asr/runtime/runtime.h"
#include "flashlight/ext/common/SequentialBuilder.h"
#include "flashlight/ext/common/Serializer.h"
#include "flashlight/ext/plugin/ModulePlugin.h"
#include "flashlight/lib/common/ProducerConsumerQueue.h"
#include "flashlight/lib/text/decoder/LexiconDecoder.h"
#include "flashlight/lib/text/decoder/LexiconFreeDecoder.h"
#include "flashlight/lib/text/decoder/LexiconFreeSeq2SeqDecoder.h"
#include "flashlight/lib/text/decoder/LexiconSeq2SeqDecoder.h"
#include "flashlight/lib/text/decoder/lm/ConvLM.h"
#include "flashlight/lib/text/decoder/lm/KenLM.h"
#include "flashlight/lib/text/decoder/lm/ZeroLM.h"

#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>
#include <grpcpp/create_channel.h>
#include "maum/brain/w2l/w2l.grpc.pb.h"
#include "maum/brain/w2l/w2l.pb.h"
#include "maum/brain/vad/vad.grpc.pb.h"

using namespace fl::app::asr;

using fl::ext::afToVector;
using fl::ext::Serializer;
using fl::lib::join;
using fl::lib::text::CriterionType;
using fl::lib::text::kUnkToken;

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::Status;
using grpc::StatusCode;
using grpc::ClientContext;
using grpc::ClientReaderWriter;
using google::protobuf::Empty;
using maum::brain::w2l::SpeechToText;
using maum::brain::w2l::Speech;
using maum::brain::w2l::Text;
using maum::brain::w2l::TextSegment;
using maum::brain::w2l::PosteriorGram;
using maum::brain::w2l::PosteriorGramInfo;
using maum::brain::vad::Vad;
using maum::brain::vad::Segment;

bool isShutdown = false;

class DecoderPool {
 public:
  explicit DecoderPool(std::shared_ptr<SequenceCriterion>& criterion, fl::lib::text::LexiconMap& lexicon,
                       fl::lib::text::Dictionary& wordDict, fl::lib::text::Dictionary& tokenDict) {
    std::vector<float> transition;
    if (FLAGS_criterion == kAsgCriterion) {
      transition = afToVector<float>(criterion->param(0).array());
    }

    /* ===================== Decode ===================== */
    // Prepare criterion
    CriterionType criterionType = CriterionType::ASG;
    if (FLAGS_criterion == kCtcCriterion) {
      criterionType = CriterionType::CTC;
    } else if (
        FLAGS_criterion == kSeq2SeqRNNCriterion ||
        FLAGS_criterion == kSeq2SeqTransformerCriterion) {
      criterionType = CriterionType::S2S;
    } else if (FLAGS_criterion != kAsgCriterion) {
      LOG(FATAL) << "[Decoder] Invalid model type: " << FLAGS_criterion;
    }

    // Build Language Model
    int unkWordIdx = -1;

    fl::lib::text::Dictionary usrDict = tokenDict;
    if (!FLAGS_lm.empty() && FLAGS_decodertype == "wrd") {
      usrDict = wordDict;
      unkWordIdx = wordDict.getIndex(kUnkToken);
    }

    std::shared_ptr<fl::lib::text::LM> lm =
        std::make_shared<fl::lib::text::ZeroLM>();
    if (!FLAGS_lm.empty()) {
      if (FLAGS_lmtype == "kenlm") {
        lm = std::make_shared<fl::lib::text::KenLM>(FLAGS_lm, usrDict);
        if (!lm) {
          LOG(FATAL) << "[LM constructing] Failed to load LM: " << FLAGS_lm;
        }
      } else if (FLAGS_lmtype == "convlm") {
        af::setDevice(0);
        LOG(INFO) << "[ConvLM]: Loading LM from " << FLAGS_lm;
        std::shared_ptr<fl::Module> convLmModel;
        std::string convlmVersion;
        Serializer::load(FLAGS_lm, convlmVersion, convLmModel);
        if (convlmVersion != FL_APP_ASR_VERSION) {
          LOG(WARNING) << "[Convlm] Model version " << convlmVersion
                       << " and code version " << FL_APP_ASR_VERSION;
        }
        convLmModel->eval();

        auto getConvLmScoreFunc = buildGetConvLmScoreFunction(convLmModel);
        lm = std::make_shared<fl::lib::text::ConvLM>(
            getConvLmScoreFunc,
            FLAGS_lm_vocab,
            usrDict,
            FLAGS_lm_memory,
            FLAGS_beamsize);
      } else {
        LOG(FATAL) << "[LM constructing] Invalid LM Type: " << FLAGS_lmtype;
      }
    }
    LOG(INFO) << "[Decoder] LM constructed.\n";

    // Build Trie
    int blankIdx =
        FLAGS_criterion == kCtcCriterion ? tokenDict.getIndex(kBlankToken) : -1;
    int silIdx = -1;
    if (FLAGS_wordseparator != "") {
      silIdx = tokenDict.getIndex(FLAGS_wordseparator);
    }
    std::shared_ptr<fl::lib::text::Trie> trie = buildTrie(
        FLAGS_decodertype,
        FLAGS_uselexicon,
        lm,
        FLAGS_smearing,
        tokenDict,
        lexicon,
        wordDict,
        silIdx,
        FLAGS_replabel);
    LOG(INFO) << "[Decoder] Trie smeared.\n";

    if (criterionType == CriterionType::S2S) {
      FLAGS_nthread_decoder = 1;
    }
    for (int i = 0; i < FLAGS_nthread_decoder; ++i) {
      // Build Decoder
      std::shared_ptr<fl::lib::text::Decoder> decoder;
      if (FLAGS_decodertype != "wrd" && FLAGS_decodertype != "tkn") {
        LOG(FATAL) << "Unsupported decoder type: " << FLAGS_decodertype;
      }

      if (criterionType == CriterionType::S2S) {
        auto amUpdateFunc = FLAGS_criterion == kSeq2SeqRNNCriterion
                            ? buildSeq2SeqRnnAmUpdateFunction(
                criterion,
                FLAGS_decoderattnround,
                FLAGS_beamsize,
                FLAGS_attentionthreshold,
                FLAGS_smoothingtemperature)
                            : buildSeq2SeqTransformerAmUpdateFunction(
                criterion,
                FLAGS_beamsize,
                FLAGS_attentionthreshold,
                FLAGS_smoothingtemperature);
        int eosIdx = tokenDict.getIndex(fl::app::asr::kEosToken);

        if (FLAGS_decodertype == "wrd" || FLAGS_uselexicon) {
          fl::lib::text::LexiconSeq2SeqDecoderOptions opt{
              .beamSize = FLAGS_beamsize,
              .beamSizeToken = FLAGS_beamsizetoken,
              .beamThreshold = FLAGS_beamthreshold,
              .lmWeight = FLAGS_lmweight,
              .wordScore = FLAGS_wordscore,
              .eosScore = FLAGS_eosscore,
              .logAdd = FLAGS_logadd,
          };
          decoder = std::make_shared<fl::lib::text::LexiconSeq2SeqDecoder>(
              opt,
              trie,
              lm,
              eosIdx,
              amUpdateFunc,
              FLAGS_maxdecoderoutputlen,
              FLAGS_decodertype == "tkn");
          if (i == 0)
            LOG(INFO) << "[Decoder] LexiconSeq2Seq decoder with "
                      << FLAGS_decodertype << "-LM loaded";
        } else {
          fl::lib::text::LexiconFreeSeq2SeqDecoderOptions opt{
              .beamSize = FLAGS_beamsize,
              .beamSizeToken = FLAGS_beamsizetoken,
              .beamThreshold = FLAGS_beamthreshold,
              .lmWeight = FLAGS_lmweight,
              .eosScore = FLAGS_eosscore,
              .logAdd = FLAGS_logadd,
          };
          decoder = std::make_shared<fl::lib::text::LexiconFreeSeq2SeqDecoder>(
              opt,
              lm,
              eosIdx,
              amUpdateFunc,
              FLAGS_maxdecoderoutputlen);
          if (i == 0)
            LOG(INFO)
              << "[Decoder] LexiconFreeSeq2Seq decoder with token-LM loaded";
        }
      } else {
        if (FLAGS_decodertype == "wrd" || FLAGS_uselexicon) {
          fl::lib::text::LexiconDecoderOptions opt{.beamSize = FLAGS_beamsize,
              .beamSizeToken = FLAGS_beamsizetoken,
              .beamThreshold = FLAGS_beamthreshold,
              .lmWeight = FLAGS_lmweight,
              .wordScore = FLAGS_wordscore,
              .unkScore = FLAGS_unkscore,
              .silScore = FLAGS_silscore,
              .logAdd = FLAGS_logadd,
              .criterionType = criterionType
          };
          decoder = std::make_shared<fl::lib::text::LexiconDecoder>(
              opt,
              trie,
              lm,
              silIdx,
              blankIdx,
              unkWordIdx,
              transition,
              FLAGS_decodertype == "tkn");
          if (i == 0)
            LOG(INFO) << "[Decoder] Lexicon decoder with " << FLAGS_decodertype
                      << "-LM loaded";
        } else {
          fl::lib::text::LexiconFreeDecoderOptions opt{.beamSize = FLAGS_beamsize,
              .beamSizeToken = FLAGS_beamsizetoken,
              .beamThreshold = FLAGS_beamthreshold,
              .lmWeight = FLAGS_lmweight,
              .wordScore = FLAGS_wordscore,
              .silScore = FLAGS_silscore,
              .logAdd = FLAGS_logadd,
              .criterionType = criterionType
          };
          decoder = std::make_shared<fl::lib::text::LexiconFreeDecoder>(
              opt,
              lm,
              silIdx,
              blankIdx,
              transition);
          if (i == 0)
            LOG(INFO)
                << "[Decoder] Lexicon-free decoder with token-LM loaded";
        }
      }
      decoders_.emplace_back(decoder);
    }
  }
  std::shared_ptr<fl::lib::text::Decoder> pop() {
    while (true) {
      {
        std::lock_guard<std::mutex> guard(mutex_lock_);
        if (!decoders_.empty()) {
          auto decoder = decoders_.back();
          decoders_.pop_back();
          return decoder;
        }
      }
      usleep(100);
    }
  }
  void push(std::shared_ptr<fl::lib::text::Decoder>& decoder) {
    std::lock_guard<std::mutex> guard(mutex_lock_);
    decoders_.emplace_back(decoder);
  }

 private:
  std::vector<std::shared_ptr<fl::lib::text::Decoder>> decoders_;
  std::mutex mutex_lock_;
};

struct Emission {
  std::vector<float> data;
  int N = 0;
  int T = 0;
  double duration = 0;
};

struct NetworkInput {
  ServerFeatures feature;
  std::chrono::steady_clock::time_point order;
  std::shared_ptr<std::promise<Emission>> p;
};

bool operator<(const NetworkInput& networkInput1, const NetworkInput& networkInput2) {
  return networkInput1.order > networkInput2.order;
}

struct DurationSet {
  std::chrono::steady_clock::time_point start;
  double vad = 0;
  double nn = 0;
  double am = 0;
  double lm = 0;
};

class NetworkThread {
 public:
  explicit NetworkThread(
      std::shared_ptr<fl::Module>& network,
      std::function<ServerFeatures(std::vector<float>& input, int channels, int time)>& inputTransform
      ) : network_(network), stop_(false) {
    thread_ = std::thread([this, &inputTransform]() {
      af::setDevice(0);
      fl::MemoryManagerInstaller::installDefaultMemoryManager();
      if (FLAGS_use_fp16)
        fl::OptimMode::get().setOptimLevel(fl::OptimLevel::O3);
      warmup_(inputTransform);
      while (true) {
        std::vector<NetworkInput> networkInputs;
        std::vector<ServerFeatures> feats;
        {
          std::unique_lock<std::mutex> guard(this->mutex_lock_);
          this->condition_.wait(guard, [this]{ return this->stop_ || !this->priorityQueue_.empty(); });
          if (this->stop_ && this->priorityQueue_.empty())
            return;
          networkInputs.emplace_back(this->priorityQueue_.top());
          feats.emplace_back(networkInputs[0].feature);
          this->priorityQueue_.pop();

          long maxFrame = networkInputs[0].feature.inputDims[0];
          while (!this->priorityQueue_.empty() && networkInputs.size() < FLAGS_max_server_batchsize) {
            NetworkInput networkInput = this->priorityQueue_.top();
            if (maxFrame < networkInput.feature.inputDims[0]) {
              maxFrame = networkInput.feature.inputDims[0];
            }
            long inputSize = maxFrame * (networkInputs.size() + 1);
            if (FLAGS_maxchunkframe < inputSize) {
              break;
            }
            networkInputs.emplace_back(networkInput);
            feats.emplace_back(networkInput.feature);
            this->priorityQueue_.pop();
          }
          VLOG(1) << "nn priority queue size: " << this->priorityQueue_.size() << ", networkInputs size: " << networkInputs.size();
        }
        std::chrono::steady_clock::time_point start;
        if (VLOG_IS_ON(1)) {
          start = std::chrono::steady_clock::now();
        }
        af::array feature = makeFeature_(feats);
        fl::Variable rawEmission;
        if (feature.dims(0) <= FLAGS_maxchunkframe) {
          rawEmission = this->network_->forward({fl::input(feature)}).front();
        } else {
          std::vector <fl::Variable> rawEmissions;
          for (int start = 0; start < (int) feature.dims(0); start += FLAGS_maxchunkframe) {
            int end = std::min(start + FLAGS_maxchunkframe, (int) feature.dims(0)) - 1;
            af::array featureChunk = feature.rows(start, end);
            auto rawEmissionChunk = this->network_->forward({fl::input(featureChunk)}).front();
            rawEmissions.emplace_back(rawEmissionChunk);
          }
          rawEmission = fl::concatenate(rawEmissions, 1);
        }
        if (FLAGS_use_fp16)
          rawEmission = rawEmission.as(af::dtype::f32);
        int networkInputIdx = 0;
        int N = rawEmission.dims(0);
        for (auto& networkInput : networkInputs) {
          int T = networkInput.feature.inputDims[0];

          auto rawEmission_i = rawEmission.slice(networkInputIdx).cols(0, T - 1);
          auto emission = afToVector<float>(rawEmission_i);
          Emission ret;
          ret.data = emission;
          ret.N = N;
          ret.T = T;
          if (VLOG_IS_ON(1)) {
            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
            std::chrono::duration<double> duration = end - start;
            ret.duration = duration.count();
          }
          networkInput.p->set_value(ret);
          networkInputIdx++;
        }
      }
    });
  };

  std::future<Emission> enqueue(NetworkInput& networkInput) {
    networkInput.p = std::make_shared<std::promise<Emission>>();
    std::future<Emission> res = networkInput.p->get_future();
    {
      std::unique_lock<std::mutex> guard(mutex_lock_);
      if(stop_)
        throw std::runtime_error("enqueue on stopped ThreadPool");
      priorityQueue_.emplace(networkInput);
    }
    condition_.notify_one();
    return res;
  };

  ~NetworkThread() {
    {
      std::unique_lock<std::mutex> lock(mutex_lock_);
      stop_ = true;
    }
    condition_.notify_all();
    thread_.join();
  }

 private:
  std::shared_ptr<fl::Module> network_;
  std::mutex mutex_lock_;
  std::priority_queue<NetworkInput> priorityQueue_;
  std::condition_variable condition_;
  std::thread thread_;
  bool stop_;

  void warmup_(
      std::function<ServerFeatures(std::vector<float>& input, int channels, int time)>& inputTransform
      ) {
    long maxAudioLength = FLAGS_samplerate * FLAGS_maxchunkframe * FLAGS_framestridems / 1000;
    std::vector<float> audio(maxAudioLength);

    ServerFeatures feat = inputTransform(audio, 1, audio.size());
    std::vector<ServerFeatures> feats;
    feats.emplace_back(feat);
    af::array feature = makeFeature_(feats);
    this->network_->forward({fl::input(feature)}).front();
  }

  af::array makeFeature_(std::vector<ServerFeatures>& feats) {
    std::vector<af::array> features;
    for (auto& feat : feats) {
      auto inputDims = af::dim4(
          feat.inputDims[0],
          feat.inputDims[1],
          feat.inputDims[2],
          feat.inputDims[3]
      );
      af::array feature = af::array(inputDims, feat.data.data());
      features.emplace_back(feature);
    }
    auto result = fl::join(features, 0, 3);
    if (FLAGS_use_fp16)
      result = result.as(af::dtype::f16);
    return result;
  }
};

struct SpeechPosition {
  unsigned long audioStart;
  unsigned long audioEnd;
  int segmentStart;
  int segmentEnd;
};

class SpeechQueue {
 public:
  void push(const std::string& data) {
    std::lock_guard<std::mutex> guard(mutex_lock_);
    data_.append(data);
  }
  std::string pop(int start, int end) {
    std::lock_guard<std::mutex> guard(mutex_lock_);
    int speechLength = end - start;
    std::string returnData = data_.substr(start - offset_, speechLength);
    data_.erase(0, end - offset_);
    offset_ = end;
    return returnData;
  }

 private:
  std::string data_;
  int offset_ = 0;
  std::mutex mutex_lock_;
};


class SpeechToTextImpl final : public SpeechToText::Service {
 public:
  explicit SpeechToTextImpl(int argc, char** argv, const std::string& flagsfile) {
    /* ===================== Create Network ===================== */
    std::shared_ptr<SequenceCriterion> criterion;
    std::unordered_map<std::string, std::string> cfg;

    /* Using acoustic model */
    LOG(INFO) << "[Network] Reading acoustic model from " << FLAGS_am;
    af::setDevice(0);
    std::string version;
    Serializer::load(FLAGS_am, version, cfg, network_, criterion);
    network_->eval();
    if (version != FL_APP_ASR_VERSION) {
      LOG(WARNING) << "[Network] Model version " << version
                   << " and code version " << FL_APP_ASR_VERSION;
    }
    LOG(INFO) << "[Network] " << network_->prettyString();
    if (criterion) {
      criterion->eval();
      LOG(INFO) << "[Criterion] " << criterion->prettyString();
    }
    LOG(INFO) << "[Network] Number of params: " << numTotalParams(network_);

    auto flags = cfg.find(kGflags);
    if (flags == cfg.end()) {
      LOG(FATAL) << "[Network] Invalid config loaded from " << FLAGS_am;
    }
    LOG(INFO) << "[Network] Updating flags from config file: " << FLAGS_am;
    gflags::ReadFlagsFromString(flags->second, gflags::GetArgv0(), true);

    // override with user-specified flags
    gflags::ParseCommandLineFlags(&argc, &argv, false);
    if (!flagsfile.empty()) {
      gflags::ReadFromFlagsFile(flagsfile, argv[0], true);
      // Re-parse command line flags to override values in the flag file.
      gflags::ParseCommandLineFlags(&argc, &argv, false);
    }

    LOG(INFO) << "Gflags after parsing \n" << serializeGflags("; ");

    /* ===================== Create Dictionary ===================== */
    auto dictPath = FLAGS_tokens;
    if (dictPath.empty() || !fl::lib::fileExists(dictPath)) {
      throw std::runtime_error("Invalid dictionary filepath specified.");
    }
    tokenDict_ = fl::lib::text::Dictionary(dictPath);
    // Setup-specific modifications
    for (int64_t r = 1; r <= FLAGS_replabel; ++r) {
      tokenDict_.addEntry("<" + std::to_string(r) + ">");
    }
    // ctc expects the blank label last
    if (FLAGS_criterion == kCtcCriterion) {
      tokenDict_.addEntry(kBlankToken);
    }
    isSeq2seqCrit_ = FLAGS_criterion == kSeq2SeqTransformerCriterion ||
                         FLAGS_criterion == kSeq2SeqRNNCriterion;
    if (FLAGS_eostoken) {
      tokenDict_.addEntry(kEosToken);
    } else if (isSeq2seqCrit_) {
      tokenDict_.addEntry(fl::app::asr::kEosToken);
      tokenDict_.addEntry(fl::lib::text::kPadToken);
    }

    int numClasses = tokenDict_.indexSize();
    LOG(INFO) << "Number of classes (network): " << numClasses;

    fl::lib::text::LexiconMap lexicon;
    if (!FLAGS_lexicon.empty()) {
      lexicon = fl::lib::text::loadWords(FLAGS_lexicon, FLAGS_maxword);
      wordDict_ = fl::lib::text::createWordDict(lexicon);
      LOG(INFO) << "Number of words: " << wordDict_.indexSize();
    }

    /* ===================== Create Data Loader ===================== */
    fl::lib::audio::FeatureParams featParams(
        FLAGS_samplerate,
        FLAGS_framesizems,
        FLAGS_framestridems,
        FLAGS_filterbanks,
        FLAGS_lowfreqfilterbank,
        FLAGS_highfreqfilterbank,
        FLAGS_mfcccoeffs,
        kLifterParam /* lifterparam */,
        FLAGS_devwin /* delta window */,
        FLAGS_devwin /* delta-delta window */);
    featParams.useEnergy = false;
    featParams.usePower = false;
    featParams.zeroMeanFrame = false;
    FeatureType featType =
        getFeatureType(FLAGS_features_type, FLAGS_channels, featParams).second;
    inputTransform_ = inputServerFeatures(
        featParams,
        featType,
        {FLAGS_localnrmlleftctx, FLAGS_localnrmlrightctx});

    /* ===================== Create Pool ==================== */
    lmPool_ = std::make_shared<DecoderPool>(criterion, lexicon, wordDict_, tokenDict_);
    networkThread_ = std::make_shared<NetworkThread>(network_, inputTransform_);

    /* ===================== Setup Detail Segment ==================== */
    useDetailSegment_ = (FLAGS_eostoken || FLAGS_usedetailsegment) && FLAGS_separatedvadresult;
    if (tokenDict_.contains(kEosToken) && FLAGS_eostoken) {
      eosIdx_ = tokenDict_.getIndex(kEosToken);
    }
    blankIdx_ = tokenDict_.getIndex(FLAGS_wordseparator);
    if (tokenDict_.contains("#") && FLAGS_usemecab) {
      mecabIdx_ = tokenDict_.getIndex("#");
    }

    frameSize_ = FLAGS_framestridems * (FLAGS_samplerate / 2000);
    frameOffset_ = FLAGS_framesizems * (FLAGS_samplerate / 1000);

    /* ===================== Create VAD Stub ==================== */
    vadStub_ = Vad::NewStub(
        grpc::CreateChannel(
            FLAGS_vadremote,
            grpc::InsecureChannelCredentials()
        )
    );
  }

  Status GetPosteriorGramInfo(ServerContext* context, const Empty* empty, PosteriorGramInfo* posteriorGramInfo) override {
    try {
      for (size_t charIdx = 0 ; charIdx < tokenDict_.indexSize(); charIdx++) {
        std::string token = tokenDict_.getEntry(charIdx);
        posteriorGramInfo->add_tokens(token);
      }
      return Status::OK;
    } catch (const std::exception& exc) {
      LOG(ERROR) << "Error while writing logs: " << exc.what();
      return Status(StatusCode::UNKNOWN, exc.what());
    }
  }

  Status ComputePosteriorGram(ServerContext* context, ServerReader<Speech>* reader, PosteriorGram* posteriorGram) override {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    try {
      Speech speech;
      std::string audio;

      while (reader->Read(&speech)) {
        audio.append(speech.bin());
      }

      auto emission = runNetwork(audio, now);

      posteriorGram->set_n(emission.N);
      posteriorGram->set_t(emission.T);
      for (auto data : emission.data) {
        posteriorGram->add_logit(data);
      }

      return Status::OK;
    } catch (const std::exception& exc) {
      LOG(ERROR) << "Error while writing logs: " << exc.what();
      return Status(StatusCode::UNKNOWN, exc.what());
    }
  }

  Status Recognize(ServerContext* context, ServerReader<Speech>* reader, Text* text) override {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    try {
      Speech speech;
      std::string audio;

      while (reader->Read(&speech)) {
        audio.append(speech.bin());
      }

      auto emission = runNetwork(audio, now);

      auto decoder = lmPool_->pop();
      std::vector<fl::lib::text::DecodeResult> results;
      try {
        results = decoder->decode(emission.data.data(), emission.T, emission.N);
      } catch (const std::exception& exc) {
        lmPool_->push(decoder);
        throw;
      }
      lmPool_->push(decoder);

      std::string txt = cleanupPrediction(results[0]);
      text->set_txt(txt);
      VLOG(2) << "output text: " << txt;
      if (VLOG_IS_ON(1)) {
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        std::chrono::duration<double> duration = end - now;
        VLOG(1) << "total runtime(s): " << duration.count();
      }
      return Status::OK;
    } catch (const std::exception& exc) {
      LOG(ERROR) << "Error while writing logs: " << exc.what();
      return Status(StatusCode::UNKNOWN, exc.what());
    }
  }

  Status StreamRecognize(ServerContext *context, ServerReaderWriter<TextSegment, Speech> *stream) override {
    std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
    DurationSet durationSet;
    if (VLOG_IS_ON(1)) {
      durationSet.start = now;
    }
    unsigned long speechLength = 0;
    unsigned long activeLength = 0;
    try {
      ClientContext clientContext;
      std::shared_ptr<ClientReaderWriter<maum::brain::vad::Speech, Segment>> vadStream(vadStub_->Detect(&clientContext));

      SpeechQueue speechQueue;
      auto asyncWriter = std::async(std::launch::async, [vadStream, stream, &speechQueue, &speechLength, &durationSet, &now]() {
        Speech speech;
        maum::brain::vad::Speech vadSpeech;
        while (stream->Read(&speech)) {
          speechQueue.push(speech.bin());
          speechLength += speech.bin().size();
          vadSpeech.set_bin(speech.bin());
          vadStream->Write(vadSpeech);
        }
        vadStream->WritesDone();
        if (VLOG_IS_ON(1)) {
          std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
          std::chrono::duration<double> vadDuration = end - now;
          durationSet.vad = vadDuration.count();
        }
      });

      Segment segment;
      auto decoder = lmPool_->pop();
      try {
        decoder->decodeBegin();
        std::vector<SpeechPosition> speechPositions;
        std::string audios;
        while (vadStream->Read(&segment)) {
          VLOG(2) << "vad start: " << segment.start() << ", end: " << segment.end();
          std::string audio = speechQueue.pop(segment.start() * 2, segment.end() * 2);
          activeLength += audio.size();
          SpeechPosition speechPosition = {
              audios.size(), audios.size() + audio.size(),
              segment.start(), segment.end()
          };
          speechPositions.emplace_back(speechPosition);
          audios.append(audio);
          if (FLAGS_minchunkaudio < audios.size()) {
            streamRecognize(audios, decoder, speechPositions, stream, now, durationSet);
            speechPositions.clear();
            audios.clear();
          }
        }
        if (!audios.empty()) {
          streamRecognize(audios, decoder, speechPositions, stream, now, durationSet);
        }
      } catch (const std::exception& exc) {
        decoder->decodeEnd();
        lmPool_->push(decoder);
        throw;
      }
      decoder->decodeEnd();
      lmPool_->push(decoder);
      asyncWriter.wait();
      Status vadStatus = vadStream->Finish();
      if (!vadStatus.ok()) {
        std::string errMsg = vadStatus.error_message();
        LOG(ERROR) << "VAD Detect rpc error msg:" << errMsg;
        return Status(StatusCode::UNKNOWN, "vad error");
      }
      if (VLOG_IS_ON(1)) {
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        std::chrono::duration<double> totalDuration = end - now;
        VLOG(1) << "total runtime(s)\t" << totalDuration.count()
                << "\tactive rate\t" << (double) activeLength / speechLength
                << "\tvad runtime(s)\t" << durationSet.vad
                << "\tam runtime(s)\t" << durationSet.am
                << "\tnn runtime(s)\t" << durationSet.nn
                << "\tlm runtime(s)\t" << durationSet.lm
            ;
      }
      return Status::OK;
    } catch (const std::exception& exc) {
      LOG(ERROR) << "Error while writing logs: " << exc.what();
      return Status(StatusCode::UNKNOWN, exc.what());
    }
  }

 private:
  std::shared_ptr<fl::Module> network_;
  fl::lib::text::Dictionary wordDict_;
  fl::lib::text::Dictionary tokenDict_;
  std::shared_ptr<DecoderPool> lmPool_;
  std::shared_ptr<NetworkThread> networkThread_;
  std::shared_ptr<Vad::Stub> vadStub_;
  int eosIdx_ = -1;
  int blankIdx_ = -1;
  int mecabIdx_ = -1;
  bool isSeq2seqCrit_ = false;

  long frameSize_;
  long frameOffset_;

  bool useDetailSegment_;

  std::function<ServerFeatures(std::vector<float>& input, int channels, int time)> inputTransform_;

  void streamRecognize(std::string& audio, std::shared_ptr<fl::lib::text::Decoder>& decoder,
                       std::vector<SpeechPosition>& speechPositions, ServerReaderWriter<TextSegment, Speech> *stream,
                       std::chrono::steady_clock::time_point& now, DurationSet& durationSet) {
    auto firstSpeechPosition = speechPositions.front();
    int offsetMs = 1000 * firstSpeechPosition.segmentStart / (int)FLAGS_samplerate;
    auto order = now + std::chrono::milliseconds(offsetMs);
    auto emission = runNetwork(audio, order, true);
    if (VLOG_IS_ON(1)) {
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      std::chrono::duration<double> amDuration = end - durationSet.start;
      durationSet.am += amDuration.count();
      durationSet.nn += emission.duration;
      durationSet.start = end;
    }
    int subStart = 0;
    for (auto& speechPosition : speechPositions) {
      int subEnd = 1 + (int)floor(((double)speechPosition.audioEnd / 2.0 - (double)frameOffset_) / (double)frameSize_ / 2.0);

      Emission subEmission;
      subEmission.N = emission.N;
      subEmission.T = subEnd - subStart;
      subEmission.data = std::vector<float>(
          emission.data.begin() + emission.N * subStart,
          emission.data.begin() + emission.N * subEnd);

      decoder->decodeStep(subEmission.data.data(), subEmission.T, subEmission.N);
      fl::lib::text::DecodeResult result = decoder->getBestHypothesis();
      decoder->prune();

      subStart = subEnd;
      if (useDetailSegment_) {
        unsigned long segmentStart = speechPosition.segmentStart;

        bool isSkip = true;
        bool isBlankStart = false;
        bool isEosStart = false;
        unsigned long eosStart = 0;
        unsigned long textStart = 0;
        for (unsigned long position = 0; position < result.tokens.size(); position++) {
          auto token = result.tokens[position];
          if (isSkip) {
            if (token == eosIdx_ || token == blankIdx_) {
              continue;
            } else {
              isSkip = false;
              segmentStart = speechPosition.segmentStart + 2 * position * frameSize_ - frameOffset_;
              if (segmentStart < speechPosition.segmentStart) {
                segmentStart = speechPosition.segmentStart;
              }
            }
          }
          if (!isBlankStart && ( token == eosIdx_ || token == blankIdx_ ) ) {
            isBlankStart = true;
            isEosStart = (token == eosIdx_);
            eosStart = position;
          } else if (isBlankStart && !isEosStart && token == eosIdx_ ) {
            isEosStart = true;
          } else if (isBlankStart && token == mecabIdx_) {
            isBlankStart = false;
            isEosStart = false;
          } else if (isBlankStart && token != eosIdx_ && token != blankIdx_ && (FLAGS_usedetailsegment || isEosStart)) {
            isBlankStart = false;
            isEosStart = false;
            unsigned long segmentEnd = speechPosition.segmentStart + (eosStart + position) * frameSize_ + frameOffset_;
            if (speechPosition.segmentEnd < segmentEnd) {
              segmentEnd = speechPosition.segmentEnd;
            }

            fl::lib::text::DecodeResult resultSegment;
            resultSegment.tokens = std::vector<int>(
                result.tokens.begin()+textStart, result.tokens.begin()+position);
            resultSegment.words = std::vector<int>(
                result.words.begin()+textStart, result.words.begin()+position);
            writeSegment(resultSegment, segmentStart, segmentEnd, stream);

            segmentStart = speechPosition.segmentStart + 2 * position * frameSize_ - frameOffset_;
            if (segmentStart < segmentEnd) {
              segmentStart = segmentEnd;
            }
            textStart = position;
          }
        }
        fl::lib::text::DecodeResult resultSegment;
        resultSegment.tokens = std::vector<int>(
            result.tokens.begin()+textStart, result.tokens.end());
        resultSegment.words = std::vector<int>(
            result.words.begin()+textStart, result.words.end());

        writeSegment(resultSegment, segmentStart, speechPosition.segmentEnd, stream);
      } else {
        writeSegment(result, speechPosition.segmentStart, speechPosition.segmentEnd, stream);
      }
    }
    if (VLOG_IS_ON(1)) {
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      std::chrono::duration<double> lmDuration = end - durationSet.start;
      durationSet.lm += lmDuration.count();
      durationSet.start = end;
    }
  }
  void writeSegment(fl::lib::text::DecodeResult& result, unsigned long segmentStart, unsigned long segmentEnd,
                    ServerReaderWriter<TextSegment, Speech>* stream) {
    std::string txt = cleanupPrediction(result);
    if (!txt.empty()) {
      TextSegment textSegment;
      textSegment.set_txt(txt);
      textSegment.set_start(segmentStart);
      textSegment.set_end(segmentEnd);

      stream->Write(textSegment);
      VLOG(2) << "(start, end) = (" << segmentStart << ", " << segmentEnd << "), output text: " << txt;
    }
  }
  Emission runNetwork(
      std::string& audio, std::chrono::steady_clock::time_point& order, bool isRaw=false) {
    VLOG(2) << "order: " << order.time_since_epoch().count();
    VLOG(2) << "input size: " << audio.size();
    std::istringstream audiois(audio);
    std::vector<float> input = loadSound<float>(audiois, isRaw);
    ServerFeatures feature = inputTransform_(input, 1, input.size());

    VLOG(2) << "feature size(N, T): " << feature.inputDims[1] << ", " << feature.inputDims[0];

    NetworkInput networkInput;
    networkInput.feature = feature;
    networkInput.order = order;
    auto result = networkThread_->enqueue(networkInput);
    auto ret = result.get();
    VLOG(2) << "emission size(N, T): " << ret.N << ", " << ret.T;
    return ret;
  }
  std::string cleanupPrediction(fl::lib::text::DecodeResult& result) {
    auto& rawWordPrediction = result.words;
    auto& rawTokenPrediction = result.tokens;

    std::vector<std::string> wordPrediction;
    if (FLAGS_uselexicon) {
      rawWordPrediction = validateIdx(rawWordPrediction, wordDict_.getIndex(kUnkToken));
      wordPrediction = wrdIdx2Wrd(rawWordPrediction, wordDict_);
    } else {
      auto letterPrediction = tknPrediction2Ltr(
          rawTokenPrediction,
          tokenDict_,
          FLAGS_criterion,
          FLAGS_surround,
          isSeq2seqCrit_,
          FLAGS_replabel,
          FLAGS_usewordpiece,
          FLAGS_wordseparator);
      wordPrediction = tkn2Wrd(letterPrediction, FLAGS_wordseparator);
    }

    auto wordPredictionStr = join(" ", wordPrediction);
    if (FLAGS_usemecab) {
      wordPredictionStr = std::regex_replace(wordPredictionStr, std::regex(" *#"), "");
    }
    if (FLAGS_usenormalizer) {
      wordPredictionStr = nfkc(wordPredictionStr);
    }
    if (FLAGS_eostoken) {
      wordPredictionStr = std::regex_replace(wordPredictionStr, std::regex(" *[$]+ *"), "\n");
      wordPredictionStr = std::regex_replace(wordPredictionStr, std::regex("^ +| +$|^\n$"), "");
    }
    return wordPredictionStr;
  }
};

int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();
  std::string exec(argv[0]);
  std::vector<std::string> argvs;
  for (int i = 0; i < argc; i++) {
    argvs.emplace_back(argv[i]);
  }
  gflags::SetUsageMessage("Usage: Please refer to https://git.io/fjVVq");
  if (argc <= 1) {
    LOG(FATAL) << gflags::ProgramUsage();
  }

  /* ===================== Parse Options ===================== */
  LOG(INFO) << "Parsing command line flags";
  gflags::ParseCommandLineFlags(&argc, &argv, false);
  auto flagsfile = FLAGS_flagsfile;
  if (!flagsfile.empty()) {
    LOG(INFO) << "Reading flags from file " << flagsfile;
    gflags::ReadFromFlagsFile(flagsfile, argv[0], true);
    // Re-parse command line flags to override values in the flag file.
    gflags::ParseCommandLineFlags(&argc, &argv, false);
  }

  /* ===================== Make gRPC Server ===================== */
  std::string server_address("0.0.0.0:");
  server_address += std::to_string(FLAGS_port);
  SpeechToTextImpl service(argc, argv, flagsfile);

  grpc::EnableDefaultHealthCheckService(true);
  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);
  std::unique_ptr<Server> server(builder.BuildAndStart());
  LOG(INFO) << "Server listening on " << server_address << std::endl;
  std::signal(SIGTERM, [](int signal) {
    isShutdown = true;
  });
  while (true) {
    if (isShutdown) {
      LOG(INFO) << "Server shutdown...";
      auto healthCheckService = server->GetHealthCheckService();
      healthCheckService->Shutdown();
      std::chrono::system_clock::time_point deadline =
          std::chrono::system_clock::now() + std::chrono::milliseconds(FLAGS_shutdown_deadline_ms);
      server->Shutdown(deadline);
      return 143;
    }
    sleep(1);
  }
  return 0;
}
