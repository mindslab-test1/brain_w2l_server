/*
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

#pragma once

#include <memory>

#include "flashlight/lib/text/decoder/lm/LM.h"
#include "flashlight/lib/text/dictionary/Dictionary.h"
#include <kenlm/lm/state.hh>

// Forward declarations to avoid including KenLM headers
namespace lm {
namespace base {

struct Vocabulary;
struct Model;

} // namespace base
namespace ngram {

struct State;

} // namespace ngram
} // namespace lm

namespace fl {
namespace lib {
namespace text {

/**
 * KenLMState is a state object from KenLM, which  contains context length,
 * indicies and compare functions
 * https://github.com/kpu/kenlm/blob/master/lm/state.hh.
 */
struct KenLMState : LMState {
  KenLMState();
  std::unique_ptr<lm::ngram::State> ken_;
  lm::ngram::State* ken() {
    return ken_.get();
  }

  int compare(const std::shared_ptr<LMState>& state) const override {
    auto ken1 = this->ken_.get();
    auto ken2 = std::static_pointer_cast<KenLMState>(state)->ken();
    if (ken1 == ken2) {
      return 0;
    }
    return ken1->Compare(*ken2);
  }
};

/**
 * KenLM extends LM by using the toolkit https://kheafield.com/code/kenlm/.
 */
class KenLM : public LM {
 public:
  KenLM(const std::string& path, const Dictionary& usrTknDict);

  LMStatePtr start(bool startWithNothing) override;

  std::pair<LMStatePtr, float> score(
      const LMStatePtr& state,
      const int usrTokenIdx) override;

  std::pair<LMStatePtr, float> finish(const LMStatePtr& state) override;

 private:
  std::shared_ptr<lm::base::Model> model_;
  const lm::base::Vocabulary* vocab_;
};

using KenLMPtr = std::shared_ptr<KenLM>;
} // namespace text
} // namespace lib
} // namespace fl
