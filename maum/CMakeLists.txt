cmake_minimum_required(VERSION 3.10)

# Main library
add_library(
  maum
  INTERFACE
)

target_sources(
  maum
  INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/brain/w2l/w2l.grpc.pb.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/brain/w2l/w2l.pb.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/brain/vad/vad.grpc.pb.cc
  ${CMAKE_CURRENT_SOURCE_DIR}/brain/vad/vad.pb.cc
)

target_link_libraries(
  maum
  INTERFACE
  ${GRPC_LIBRARIES}
)
